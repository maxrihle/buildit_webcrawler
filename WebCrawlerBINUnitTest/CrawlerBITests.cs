﻿using NUnit.Framework;
using WebCrawlerBI;

using static Tests.TestConstants;


namespace Tests
{
    [TestFixture]
    [Category("CrawlerBITests")]
    public class CrawlerBITests
    {
        [SetUp]
        public void Setup()
        {
            //Assert.Fail();
        }

        /// <summary>
        /// simple run-to-the-end test
        /// </summary>
        [Test]
        public void RunOnContoso()
        {
            string address = urlFull;
            string domain = UriHelper.ExtractDomain(address);

            CrawlerBI crawler = new CrawlerBI(address, domain);
            crawler.PerformCrawling();

            Assert.Pass();
        }

        /// <summary>
        /// simple run-to-the-end test on REAL url, takes over 1 minute
        /// </summary>
        [Test]
        public void LONG_RunOnRealUrl()
        {
            string address = urlRealHttp;
            string domain = UriHelper.ExtractDomain(address);

            CrawlerBI crawler = new CrawlerBI(address, domain);
            crawler.PerformCrawling();

            Assert.Pass();
        }

        /// <summary>
        /// run on input of a domain in a short form, no protocol given explicily
        /// </summary>
        [Test]
        public void RunOnContosoWithoutProtocol()
        {
            string address = urlFullNoProtocol;
            string domain = UriHelper.ExtractDomain(address);

            CrawlerBI crawler = new CrawlerBI(address, domain);
            crawler.PerformCrawling();

            Assert.Pass();
        }

        /// <summary>
        /// run on localhost in a short form; Will fail if no web server on localhost:80 is running
        /// </summary>
        [Test]
        public void RunOnLocalhostWithoutProtocol()
        {
            string address = urlLocalhostNoProtocol;
            string domain = UriHelper.ExtractDomain(address);

            CrawlerBI crawler = new CrawlerBI(address, domain);
            crawler.PerformCrawling();


            Assert.Pass();
        }

        /// <summary>
        /// run on non-existing address, expecting exception of the type  System.Net.WebException
        /// </summary>
        [Test]
        public void RunOnNonRealAddress()
        {
            string address = urlNotExisting;
            string domain = UriHelper.ExtractDomain(address);

            try
            {
                CrawlerBI crawler = new CrawlerBI(address, domain);
                crawler.PerformCrawling();
            }
            catch (System.Net.WebException)
            {
                Assert.Pass();
            }


            Assert.Fail();
        }

        /// <summary>
        /// run on non-existing address, expecting exception of the type  System.Net.WebException
        /// </summary>
        [Test]
        public void RunOnMistypedAddress()
        {
            string address = urlMistake;
            string domain = UriHelper.ExtractDomain(address);

            try
            {
                CrawlerBI crawler = new CrawlerBI(address, domain);
                crawler.PerformCrawling();
            }
            catch (System.Net.WebException)
            {
                Assert.Pass();
            }


            Assert.Fail();
        }

    }
}
