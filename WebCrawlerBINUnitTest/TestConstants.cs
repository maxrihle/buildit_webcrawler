﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tests
{
    public static class TestConstants
    {
        #region constants

        public static string urlFull = "http://www.contoso.com";
        public static string urlFullNoProtocol = "www.contoso.com";
        public static string urlShort = "http://contoso.com";
        public static string urlShortNoPtotocol = "contoso.com";
        public static string urlNotExisting = "http://never.ever.never";
        public static string urlMistake = "http://wwwwwww.contoso.com";
        public static string urlLocalhost = "http://localhost";
        public static string urlLocalhostNoProtocol = "localhost";

        public static string urlRealHttp  = "http://wiprodigital.com";
        public static string urlRealHttps = "https://wiprodigital.com";
        #endregion
    }
}
