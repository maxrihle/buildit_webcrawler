﻿using NUnit.Framework;
using WebCrawlerBI;

using static Tests.TestConstants;



namespace Tests
{
    [TestFixture]
    [Category("UriHelperTests")]
    public class UriHelperTests
    {
        [SetUp]
        public void Setup()
        {
            //Assert.Fail();
        }

        [Test]
        public void ExtractDomainTest()
        {
            string address = urlFull;
            string domain = UriHelper.ExtractDomain(address);
            Assert.AreEqual(domain, ".contoso.com");
            Assert.AreNotEqual(domain, address);

        }

        [Test]
        public void ExtractDomainTestWithoutProtocol()
        {
            string address = urlFullNoProtocol;
            string domain = UriHelper.ExtractDomain(address);
            Assert.AreEqual(domain, ".contoso.com");
            Assert.AreNotEqual(domain, address);
        }

    }
}
