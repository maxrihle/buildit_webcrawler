using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using WebCrawlerBI;

using static Tests.TestConstants;


namespace Tests
{
    /// <summary>
    /// Consider this as integration tests
    /// </summary>
    [TestFixture]
    [Category("CrawlerBIOutputTests")]
    public class LONG_CrawlerBIOutputTests
    {
        IEnumerable<string> InternalPages;
        IEnumerable<string> LinkedPagesInDomain;
        IEnumerable<string> LinkedPagesNotInDomain;
        IEnumerable<string> StaticResources;

        int proximity = 20; // numbers may change over time

        //int estimateForInternalPages = 183;
        int estimateForLinkedPagesInDomain       = 183;
        int estimateForLinkedPagesNotInDomain    = 387;
        int estimateForStaticResources           = 890;

        /// <summary>
        /// Retrieve data for analysis
        /// </summary>
        [SetUp]
        public void Setup()
        {
            string address = urlRealHttp;
            string domain = UriHelper.ExtractDomain(address);

            CrawlerBI crawler = new CrawlerBI(address, domain);
            crawler.PerformCrawling();

            InternalPages = crawler.GetInternalPages();
            LinkedPagesInDomain = crawler.GetPagesInDomain();
            LinkedPagesNotInDomain = crawler.GetPagesNotInDomain();
            StaticResources = crawler.GetStaticResources();

            Assert.Pass();
        }


        /// <summary>
        /// simple run-to-the-end test, 
        /// </summary>
        [Test]
        public void VerifyNumbersBallpark()
        {

            int actualForInternalPages = InternalPages.Sum(page => 1);

            int actualForLinkedPagesInDomain = LinkedPagesInDomain.Sum(page => 1);
            int actualForLinkedPagesNotInDomain = LinkedPagesNotInDomain.Sum(page => 1);
            int actualForStaticResources = StaticResources.Sum(page => 1);

            Assert.AreEqual(actualForInternalPages, actualForLinkedPagesInDomain);

            Assert.LessOrEqual(Math.Abs(actualForLinkedPagesInDomain - estimateForLinkedPagesInDomain), proximity);
            Assert.LessOrEqual(Math.Abs(actualForLinkedPagesNotInDomain - estimateForLinkedPagesNotInDomain), proximity);
            Assert.LessOrEqual(Math.Abs(actualForStaticResources - estimateForStaticResources), proximity);

        }
    }
 }