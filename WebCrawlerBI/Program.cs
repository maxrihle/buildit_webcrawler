﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

// add logging log4net

namespace WebCrawlerBI
{

    class Program
    {
        // starting point
        // static readonly string startUrl = "https://buildit.wiprodigital.com/careers";
        static readonly string startUrl = "https://wiprodigital.com";
        // static readonly string domain = ".wiprodigital.com"; // notice the starting period

        static readonly string outputFile = "WebCrawlerBIlatestOutput.html";

        static void Main(string[] args) // pass startUrl as a commandline argument
        {
            // check if provided a command line parameter, if yes - use it as a starting point;

            string startingPoint = startUrl;

            if ( null != args && args.Length > 0)
            {
                startingPoint = args[0];
            }
            string theDomain = UriHelper.ExtractDomain(startingPoint);

            var crawler = new CrawlerBI(startingPoint, theDomain);

            if (Logger.Log != null) // to bypass nUnit logging during testing; //TODO: resolve
                Logger.Log.Debug($"started, using the url {startingPoint}");
            Console.WriteLine($"started, using the url {startingPoint}");
            crawler.PerformCrawling();


            string CrawlerSimpleReport = crawler.ProduceSimpleHtmlReport( true ); // want to get it ordered


            Console.WriteLine($"finished, using the url {startingPoint}.");

            // Write file using StreamWriter ; to see in console, uncomment the following line

            //Console.WriteLine(CrawlerSimpleReport);
            using (StreamWriter writer = new StreamWriter(outputFile))
            {
                writer.WriteLine( CrawlerSimpleReport );
            }

            if (Logger.Log != null) // to bypass nUnit logging during testing; //TODO: resolve
                Logger.Log.Debug($"\n\nOutput saved to the local file {outputFile};");

            Console.WriteLine($"\n\nOutput saved to the local file {outputFile}; \n\nPress a key to exit");
            Console.ReadKey();

            return;
        }

    }

}

