﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawlerBI
{
    public static class UriHelper
    {

        #region (Helper features) fix URL string, check domain

        public static void NormalizeUrl(ref string customUrl, string currentUrl, string customSeparator, 
            string masterDomain, out bool IsInDomain)
        {
            bool _isInDomain = false;
            ResolveLocalAddress(ref customUrl, currentUrl, customSeparator, masterDomain, out _isInDomain);
            // todo: set of possibel variations

            //remove any in-page location indications
            var parts = customUrl.Split('#');
            if (customUrl != parts[0])
                customUrl = parts[0].Trim();

            // remove trailing '/'
            while (customUrl.EndsWith("/"))
                customUrl = customUrl.Substring(0, customUrl.Length - 1);

            IsInDomain = _isInDomain;
        }

        public static void ResolveLocalAddress(ref string complexString, string currentUrl, string customSeparator,
            string masterDomain, out bool IsInDomain)
        {
            bool _isInDomain = false;

            // extract the 'address' from the 'customUrl' string, as it may contain prefixes.

            string[] separatingStrings = { customSeparator };
            string[] parts = complexString.Split(separatingStrings, StringSplitOptions.None);

            string customUrl = parts[parts.Length - 1]; // last one contains the actual address

            // strip framing "" or '' if necessary
            while( customUrl.StartsWith("\"") && customUrl.EndsWith("\"") || customUrl.StartsWith("'") && customUrl.EndsWith("'") )
            {
                customUrl = customUrl.Substring(1, customUrl.Length - 2);
            }

            Uri myUri = null;

            bool success = false; // trying set of ways to parse
            try
            {
                myUri = new Uri(customUrl);
                success = true;
            }
            catch
            {
                success = false;
            }

            if (!success)
                try
                {
                    myUri = new Uri(currentUrl);
                    string currentHost = myUri.Host;
                    // determine protocole
                    string[] parts2 = currentUrl.Split(':');
                    string protocol = parts2[0];

                    if( !customUrl.StartsWith("/") )
                    {
                        customUrl = "/" + customUrl;
                    }
                    string amendedPage = protocol + "://" + currentHost + customUrl;

                    myUri = new Uri(amendedPage);
                    customUrl = amendedPage;
                    success = true;
                }
                catch
                {
                    success = false;
                }

            if (!success)
                try
                {
                    string protocol = "http";

                    string amendedPage = protocol + "://" + customUrl; // to address casese like '../<smth>'

                    myUri = new Uri(amendedPage);
                    customUrl = amendedPage;
                    success = true;
                }
                catch (Exception e)
                {
                    throw e; // cannot make sense of the link, cannot get a valid URI from it.
                }

            //figure out whether the link sits within the domain
            string host = myUri.Host;
            if (host.EndsWith( masterDomain) || "." + host == masterDomain )
            {
                _isInDomain = true;
            }

            IsInDomain = _isInDomain;

            // recombine parts of the complex string

            parts[parts.Length - 1] = customUrl; // place the fixed value back in its place
            complexString = String.Join(customSeparator, parts);
        }

        public static string ExtractDomain( string customUrl)
        {
            // expeting a well formed URI 


            Uri myUri = null;

            bool success = false; // trying set of ways to parse
            try
            {
                myUri = new Uri(customUrl);
                success = true;
            }
            catch
            {
                success = false;
            }

            if (!success)
                try
                {
                    string amendedPage = "http://" + customUrl;
                    myUri = new Uri(amendedPage);
                    success = true;
                }
                catch( Exception e)
                {
                    throw e; // cannot make sense of the link, cannot get a valid URI from it.
                }

            //figure out whether the link sits within the domain
            string host = myUri.Host;

            // assumption: good host is either "localhost" or "<something>.<something>", e.g. ending with ".com".
            // remark: in case of IP address ("127.0.0.1"), the last two numbers (".0.1") will be used.
            // TODO: add intelligent resolver

            string[] parts = host.Split('.');
            switch( parts.Length )
            {
                case 1:
                    return "." + parts[0];
                case 2:
                    return "." + parts[0] + "." + parts[1];
                default:
                    return "." + parts[parts.Length - 2] + "." + parts[parts.Length - 1];
            }
        }
        #endregion
    }
}
