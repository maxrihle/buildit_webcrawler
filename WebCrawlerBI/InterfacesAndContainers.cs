﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebCrawlerBI
{

    #region interfaces
    public interface ILinkDetails
    {
        HashSet<string> LinkedFrom { get; set; }

        bool IsInDomain { get; set; }
    }
    public interface ILinkContainer
    {
        void Add(string newKey, bool IsInDomain);
        bool ContainsKey(string newKey);

        HashSet<string> LinkedFromFor(string existingKey);
        bool IsInDomainFor(string existingKey);
    }
    #endregion

    #region container classes
    public class PageDetails : LinkDetails// full hyperlinks / resources list, including 'parents'
    {
        public PageDetails(bool IsInDomain = false)
        {
            LinkedTo = new HashSet<string>();
            StaticResources = new HashSet<string>();
        }
        // List of 'origination' sites
        // public HashSet<string> LinkedFrom { get; set; }// comes from parent class

        // List of 'destination' sites 
        public HashSet<string> LinkedTo { get; set; }// all links, in- and ex- ternal

        // list of static resources: images etc
        public HashSet<string> StaticResources { get; set; } //= new HashSet<string>();
    }

    public class LinkDetails : ILinkDetails // full hyperlinks / resources list, including 'parents'
    {
        public LinkDetails(bool IsInDomain = false)
        {
            this.IsInDomain = IsInDomain;
            LinkedFrom = new HashSet<string>();
        }

        #region interface methods
        public HashSet<string> LinkedFrom { get; set; }

        public bool IsInDomain { get; set; }
        #endregion

    }//= new HashSet<string>();

    public class PageContainer : ILinkContainer
    {
        public PageContainer()
        {
            pages = new Dictionary<string, PageDetails>();
        }

        internal Dictionary<string, PageDetails> pages;
        public HashSet<string> LinkedToFor(string existingKey)
        {
            return pages[existingKey].LinkedTo;
        }

        public HashSet<string> StaticResourcesFor(string existingKey)
        {
            return pages[existingKey].StaticResources;
        }

        #region interface methods
        public bool ContainsKey(string newKey)
        {
            return pages.ContainsKey(newKey);
        }

        public void Add(string newKey, bool IsInDomain = true) // all which in this structure are in domain, so ignore the bool
        {
            pages.Add(newKey, new PageDetails());
        }

        public HashSet<string> LinkedFromFor(string existingKey)
        {
            return pages[existingKey].LinkedFrom;
        }

        public bool IsInDomainFor(string dummy)
        {
            return true; // all which in this structure are in domain
        }
        #endregion
    }
    public class LinkContainer : ILinkContainer
    {
        internal Dictionary<string, LinkDetails> links = new Dictionary<string, LinkDetails>();

        #region interface methods
        public bool ContainsKey(string newKey)
        {
            return links.ContainsKey(newKey);
        }

        public void Add(string newKey, bool IsInDomain = false)
        {
            links.Add(newKey, new LinkDetails(IsInDomain));
        }
        public HashSet<string> LinkedFromFor(string existingKey)
        {
            return links[existingKey].LinkedFrom;
        }

        public bool IsInDomainFor(string existingKey)
        {
            return links[existingKey].IsInDomain;
        }
        #endregion
    }

    #endregion

}
