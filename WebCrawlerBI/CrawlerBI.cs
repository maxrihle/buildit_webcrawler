﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
//using Logger;

namespace WebCrawlerBI
{

    /// <summary>
    /// The work is done by this class
    /// </summary>
    public class CrawlerBI
    {
        #region constants and read-only-s
        // starting point
        internal readonly string startUrl, domain; // shall be used during the life time of an instanse of the class
        const string partsSeparator = " => ";
        #endregion

        #region create data structures for retrieved results

        // stack of pages to check
        Stack<string> PagesToCheck = new Stack<string>();


        // List of in-domain sites, with a list of 'parents', 'children',

        // step-children (external sites) and static resources
        PageContainer InternalPages
            = new PageContainer();

        // List of external sites
        LinkContainer LinkedPages =
             new LinkContainer();

        // list of static resources: images, scripts, etc
        LinkContainer StaticResources =
            new LinkContainer();

        #endregion

        #region constructor(s)
        public CrawlerBI(string startUrl, string domain)
        {
            this.startUrl = startUrl;
            this.domain   = domain;
        }

        #endregion

        #region PerformCrawling: main cycle
        public void PerformCrawling()
        {
            string currentPage = startUrl;
            HandleNewLink(currentPage, null, LinkedPages, null, true);

            while (PagesToCheck.Count != 0) // work uptil stack is empty
            {
                currentPage = PagesToCheck.Pop();
                ProcessPage(currentPage);
            }

            // dump the collected data to a (set of) html / xml files

            return;
        }
        #endregion

        #region Analyze a page, extract links and resources
        internal void ProcessPage(string currentUrl)
        {
            if (Logger.Log != null) // to bypass nUnit logging during testing; //TODO: resolve
                Logger.Log.Debug($"\n==== { currentUrl  } == static resources ====================================\n");


            //nb: the url is already 'normalized'
            // get lists of hyperlinks / resources

            var doc = new HtmlWeb().Load(currentUrl);

            // 'link' - includes icons, etc.
            var linkTags = doc.DocumentNode.Descendants("link")
                                              .Select(
                link => "link" + partsSeparator + link.GetAttributeValue("rel", null) + partsSeparator +
                    link.GetAttributeValue("href", null))
                                              .Where(u =>
                                              {
                                                  string[] separatingStrings = { partsSeparator };
                                                  string[] parts = u.Split(separatingStrings, StringSplitOptions.None);
                                                  return !String.IsNullOrEmpty(u) && !String.IsNullOrEmpty(parts[2]);
                                              });

            var imgTags = doc.DocumentNode.Descendants("img")
                                  .Select(img => img.GetAttributeValue("src", null))
                                  .Where(u => !String.IsNullOrEmpty(u));

            var bgrImgTags = doc.DocumentNode.Descendants("div")                    // TODO: may check "img" or anything else
                .Where(d => d.Attributes.Contains("style") && (
                      d.Attributes["style"].Value.Contains("background:url") 
                   || d.Attributes["style"].Value.Contains("background: url") ) )   // TODO: use REGEX to address whitespace(s)
                .Select(img =>
                {
                    string style = img.Attributes["style"].Value;
                    return Regex.Match(style, @"(?<=\().+?(?=\))").Value;
                })
                .Where(u => !String.IsNullOrEmpty(u));

            var scriptTags = doc.DocumentNode.Descendants("script") 
                                  .Select(script => script.GetAttributeValue("src", null))
                                  .Where(u => !String.IsNullOrEmpty(u));

            var linkedPages = doc.DocumentNode.Descendants("a")
                                              .Select(a => a.GetAttributeValue("href", null))
                                              .Where(u => !String.IsNullOrEmpty(u));

            foreach (var link in linkTags)
            {
                HandleNewLink(link, currentUrl, StaticResources, InternalPages.StaticResourcesFor(currentUrl), false);
            }

            foreach (var imgPassed in imgTags)
            {
                var img = imgPassed;
                img = "img => " + img;
                HandleNewLink(img, currentUrl, StaticResources, InternalPages.StaticResourcesFor(currentUrl), false);
            }

            foreach (var bgrImgPassed in bgrImgTags)
            {
                var bgrImg = bgrImgPassed;
                bgrImg = "bgrImg => " + bgrImg;
                HandleNewLink(bgrImg, currentUrl, StaticResources, InternalPages.StaticResourcesFor(currentUrl), false);
            }

            foreach (var scriptPassed in scriptTags)
            {
                string script = scriptPassed;
                script = "script => " + script;
                HandleNewLink(script, currentUrl, StaticResources, InternalPages.StaticResourcesFor(currentUrl), false);
            }

            if (Logger.Log != null) // to bypass nUnit logging during testing; //TODO: resolve
                Logger.Log.Debug($"\n==== { currentUrl  } == linked pages ========================================\n");

            foreach (var page in linkedPages)
            {
                HandleNewLink(page, currentUrl, LinkedPages, InternalPages.LinkedToFor(currentUrl), true); // NB: TRUE in the end
            }

            //            Console.ReadKey();

            if (Logger.Log != null) // to bypass nUnit logging during testing; //TODO: resolve
                Logger.Log.Debug($"\n==== { currentUrl  } == done ================================================\n");

            return;
        }

        #endregion

        #region For each link, amend datasctructures
        internal void HandleNewLink(string newLink, string currentUrl,
            ILinkContainer container, HashSet<string> originPointingTo, bool mayNeedVisit = false)
        {
            bool belongsToDomain = false;
            UriHelper.NormalizeUrl(ref newLink, currentUrl, partsSeparator, this.domain, out belongsToDomain); // includes resolution of local addresses

            if (!container.ContainsKey(newLink))
            {
                container.Add(newLink, belongsToDomain);
                if (mayNeedVisit && belongsToDomain)
                {
                    // need to add to 'internal pages' structure, and to stack for visit/analysis
                    InternalPages.Add(newLink);
                    PagesToCheck.Push(newLink);
                }
            }

            if ((!(currentUrl == null)) && (!container.LinkedFromFor(newLink).Contains(currentUrl)))
            {
                container.LinkedFromFor(newLink).Add(currentUrl);
                originPointingTo.Add(newLink); // no need for check, as the two structures are spposedly in sync


                if (Logger.Log != null) // to bypass nUnit logging during testing; //TODO: resolve
                    Logger.Log.Debug($"==== {belongsToDomain} ==== { newLink }");
            }
        }

        #endregion

        #region Results retrieval methods

        public IEnumerable<string> GetInternalPages(bool ordered = false)
        {
            var theList = 
                InternalPages.pages.Select(link => link.Key);
            if (ordered)
                theList = theList.OrderBy(u => u);
            return theList;
        }
        /// <summary>
        /// Get ALL Pages In Domain
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetPagesInDomain(bool ordered = false)
        {
            var theList = 
                LinkedPages.links
                    .Where(link => link.Value.IsInDomain)
                    .Select(link => link.Key);
            if (ordered)
                theList = theList.OrderBy(u => u);
            return theList;
        }

        /// <summary>
        /// Get ALL Pages NOT In Domain
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetPagesNotInDomain(bool ordered = false)
        {
            var theList =
                LinkedPages.links
                    .Where(link => !link.Value.IsInDomain)
                    .Select(link => link.Key);
            if (ordered)
                theList = theList.OrderBy(u => u);
            return theList;
        }

        /// <summary>
        /// for a resource, get where it gets referenced from
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        public IEnumerable<string> GetLinkedFromForResource(string resource, bool ordered = false)
        {
            if (!StaticResources.ContainsKey(resource))
                return null;
            var theList =
                StaticResources.links[resource].LinkedFrom
                    .Select(link => link);
            if (ordered)
                theList = theList.OrderBy(u => u);
            return theList;
        }

        /// <summary>
        /// for a page, internal or external, get where it gets referenced from
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public IEnumerable<string> GetLinkedFromForPage(string page, bool ordered = false)
        {
            if (!LinkedPages.ContainsKey(page))
                return null;
            var theList =
                LinkedPages.links[page].LinkedFrom
                    .Select(link => link); // makes a clone
            if (ordered)
                theList = theList.OrderBy(u => u);
            return theList;
        }

        /// <summary>
        /// Get list of all INTERNAL pages referenced by a given page
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public IEnumerable<string> GetInDomainPagesLinkedToFor(string page, bool ordered = false)
        {
            if (!InternalPages.ContainsKey(page))
                return null;
            var theList =
                InternalPages.pages[page].LinkedTo
                    .Where(link => LinkedPages.IsInDomainFor(link));
            if (ordered)
                theList = theList.OrderBy(u => u);
            return theList;
        }

        /// <summary>
        ///  Get list of all EXTERNAL pages referenced by a given page
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public IEnumerable<string> GetNotInDomainPagesLinkedToFor(string page, bool ordered = false)
        {
            if (!InternalPages.ContainsKey(page))
                return null;
            var theList =
                InternalPages.pages[page].LinkedTo
                    .Where(link => !LinkedPages.IsInDomainFor(link));
            if (ordered)
                theList = theList.OrderBy(u => u);
            return theList;
        }


        /// <summary>
        ///  Get list of all static resourced referenced by a given page
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public IEnumerable<string> GetStaticResourcesLinkedToFor(string page, bool ordered = false)
        {
            if (!InternalPages.ContainsKey(page))
                return null;
            var theList =
                InternalPages.pages[page].StaticResources
                    .Select(link => link);
            if (ordered)
                theList = theList.OrderBy(u => u);
            return theList;
        }

        /// <summary>
        /// Get list of ALL static resources
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetStaticResources(bool ordered = false)
        {
            var theList = 
                StaticResources.links
                    .Select(link => link.Key);
            if( ordered )
                theList = theList.OrderBy( u => u );
            return theList;
        }

        /// <summary>
        /// Produce an HTML report
        /// </summary>
        /// <returns></returns>
        public string ProduceSimpleHtmlReport( bool ordered = false)
        {
            StringBuilder sb = new StringBuilder();
            IEnumerable<string> outputLinkedPagesInDomain = GetPagesInDomain(ordered);
            IEnumerable<string> outputInternalPages = GetInternalPages(ordered);
            IEnumerable<string> outputLinkedPagesNotInDomain = GetPagesNotInDomain(ordered);
            IEnumerable<string> outputStaticResources = GetStaticResources(ordered);

            sb.Append("<html>\n<head>simple output</head>\n<body>");


            sb.Append("\n\n<h1>Internal Pages</h1>\n");
            foreach (var link in outputLinkedPagesInDomain)
            {
                sb.Append("\n" + link + "<br>");
            }

            sb.Append("\n\n<h1>External Pages</h1>\n");
            foreach (var link in outputLinkedPagesNotInDomain)
            {
                sb.Append("\n" + link + "<br>");
            }

            sb.Append("\n\n<h1>Static Resources (All)</h1>\n");
            foreach (var link in outputStaticResources)
            {
                sb.Append("\n" + link + "<br>");
            }

            sb.Append("\n\n<h1>Pages and Static Resources (ByPage)</h1>\n");
            foreach (var link in outputLinkedPagesInDomain)
            {
                IEnumerable<string> outputInDomainPagesLinkedToFor = GetInDomainPagesLinkedToFor(link, ordered);
                IEnumerable<string> outputNotInDomainPagesLinkedToForPage = GetNotInDomainPagesLinkedToFor(link, ordered);
                IEnumerable<string> outputStaticResourcesForPage = GetStaticResourcesLinkedToFor(link, ordered);

                sb.Append($"\n\n<h2>Pages and Static Resources for <b>{link}</b></h2>\n");

                sb.Append($"\n\n<h3>Internal Pages for <b>{link}</b></h3>\n");
                foreach (var resource in outputInDomainPagesLinkedToFor)
                {
                    sb.Append("\n" + resource + "<br>");
                }

                sb.Append($"\n\n<h3>External Pages for <b>{link}</b></h3>\n");
                foreach (var resource in outputNotInDomainPagesLinkedToForPage)
                {
                    sb.Append("\n" + resource + "<br>");
                }

                sb.Append($"\n\n<h3>Static Resources for <b>{link}</b></h3>\n");
                foreach (var resource in outputStaticResourcesForPage)
                {
                    sb.Append("\n" + resource + "<br>");
                }
            }

            sb.Append("\n</body>\n</html>");

            string rtrn = sb.ToString();

            if (Logger.Log != null) // to bypass nUnit logging during testing; //TODO: resolve
                Logger.Log.Debug($"\n\n SIMPLE REPORT OTPUT \n {rtrn} \n\n=====================\n\n");

            return rtrn;
        }



        #endregion
    }
}
