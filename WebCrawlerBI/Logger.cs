﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace WebCrawlerBI
{
    public static class Logger
    {
        private static readonly log4net.ILog log = null;

        static Logger()
        {
            try
            {
                log = log4net.LogManager.GetLogger
                        (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            }
            catch( Exception e )
            {
                Console.WriteLine(e.ToString() );//TODO: here is an error of type init, happening ONLY when running nUnit tests; 
                                               // need f\urther investigation / resolution
      
            }
        }
        public static log4net.ILog Log
        {
            get { return log; }
        }
    }
}
