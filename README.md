

1. How to build, test and run

- required packages: 
	"HtmlAgilityPack", version="1.11.18"
	"log4net", version="2.0.8"

	Both packages can be added by NuGet, e.g. directly from Visual Studio 2017 IDE.
	They are also included int the GIT repo, folder "packages".

- build 
	if you use Visual Studio - e.g. VS 2017, you can load the provided 'WebCrawlerBI.sln' file.
	Two projects are included in the solution: 
		- the main project 'WebCrawlerBI'
		- the testing project 'WebCrawlerBINUnitTest'

- run		
	The application runs as a console application. It takes starting IURL as first command line parameter.
	It will produce  output file named "WebCrawlerBIlatestOutput.html", listing the findings.
	
	Output format:
		- List of all pages in the domain, ordered as strings
		- List of all linked pages outside of the domain, ordered as strings
		- List of all 'static resources', with some additional info showing whether they are images, background images, scripts, or 'links' - in the last case the 'rel' tag is also included, to clarify their role.
		
		- finally, same three lists are composed 'by page' - for each page in the domain.
		
		All the date in the same order is listed in a single HTML file, using headers for separating the sections.
	
	Example of the output file can be seen in the folder "ExampleOutput"  named  "Example_WebCrawlerBIlatestOutput_WiProDigital.com.html"
	
- test
	9 tests are provided, addressing the issues which popped up during development.
	They include:
		- unit tests for 'helper' functions dealing with domain names extracted from fond URL
		- basic sanity integration end-to-end tests for a set of good and bad input parameters
		- functional / integration test, retrieving complete data and comparing the output numbers agains benchmark precollected values
		
	They are available to run from VS 'test explorer', and can be configured to run automatically in a CI/CD environment.
		
2. tradeoffs
	- number of tests should be greater in case the project will be used in production.
		E.g. Interfaces / Data Structures were tested manually during debugging, but may need to be automated. Similarly, more automated functional tests may be added.
		Proper testing environment may include a set of web sites with different structures and syntax specifics, and benchmark for them. 
	- Background images: solution is lacking of comprehensive collection of different HTML elements which may have them. 'TODO' comments are left in source code, to be addressed eventually.
	- Output is pretty basic, but available date may permit more advanced visualization, e.g. as a directed graph.
	
	- logging is provided, but it is switched off during automated tests
	
3. what would achieve given more time
	- resolve the above tradeoffs
	- collect more details from end users / stakeholders, and elaborate the project accordingly
	